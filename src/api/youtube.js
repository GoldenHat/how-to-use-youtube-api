import axios from "axios";
const KEY = "AIzaSyAPH19WJqEmlCbbJOZumhYibdEL-7oAtbc";

export default axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3",
  params: {
    part: "snippet",
    maxResults: "50", //the numbers of Results from YouTube
    key: KEY,
  },
});
