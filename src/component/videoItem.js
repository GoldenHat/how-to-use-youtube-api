import React from "react";
import "./image-list.css";
const VideoItem = ({ video, onVideoSelect, onChannelSelect }) => {
  const items = video;

  return (
    <div>
      <div onClick={() => onVideoSelect(items)} className="ui card">
        <a className="image" href="#">
          <div className="ui column grid">
            <div className="column">
              <div className="ui fluid image">
                <div className="ui grey ribbon label" onClick={() => onChannelSelect(items.snippet.channelId)}>
                  <i className="tv icon" />
                  {items.snippet.channelTitle}
                </div>
                <img src={items.snippet.thumbnails.high.url} />
              </div>
            </div>
          </div>
        </a>

        <div className="content">
          <a className="header ui center aligned" href="#">
            {items.snippet.title}
          </a>
          <div className="meta">
            <div className="ui circular labels">
              <a className="ui label" style={{ margin: "0.2em" }}>
                <i className="calendar alternate icon" /> {new Date(items.snippet.publishedAt).toDateString()}
              </a>
            </div>
            <br />
            <div className="ui labeled button" tabIndex="0" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default VideoItem;
