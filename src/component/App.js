import React from "react";
import SearchBar from "./searchBar";
import VideoList from "./VideoList";
import youtube from "../api/youtube";
import VideoDetail from "./videoDetail";

class App extends React.Component {
  state = { videos: [], selectedVideo: null };
  onTermSubmit = term => {
    return youtube
      .get("/search", {
        params: {
          q: term,
        },
      })
      .then(item => {
        this.setState({ videos: item.data.items, selectedVideo: item.data.items[Math.floor(Math.random() * Math.floor(50))] });
      });
  };

  onVideoSelect = video => {
    this.setState({ selectedVideo: video });
  };

  render() {
    return (
      <div className="ui container">
        <SearchBar onTermSubmit={this.onTermSubmit} videoNumbers={this.state.videos} />
        <VideoDetail video={this.state.selectedVideo} />
        <VideoList onVideoSelect={this.onVideoSelect} Vlist={this.state.videos} />
      </div>
    );
  }
}

export default App;
