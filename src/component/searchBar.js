import React from "react";

class SearchBar extends React.Component {
  state = { term: "Testing!!!" };
  onInputChange = e => {
    this.setState({ term: e.target.value });
  };
  onFormSubmit = e => {
    e.preventDefault();
    this.props.onTermSubmit(this.state.term);
  };

  // AIzaSyAPH19WJqEmlCbbJOZumhYibdEL-7oAtbc

  render() {
    return (
      <div className="ui segment search-bar">
        <form onSubmit={this.onFormSubmit} className="ui form">
          <div className="field">
            <label>Search Video</label>
            <input type="text" value={this.state.term} onChange={this.onInputChange} />
            {this.props.videoNumbers.length !== 0 ? "Videos:" + this.props.videoNumbers.length + " Found" : false}
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBar;
