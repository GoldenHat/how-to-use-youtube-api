import React from "react";
import VideoItem from "./videoItem";
import "./image-list.css";

const VideoList = ({ Vlist, onVideoSelect }) => {
  const video = Vlist.map(video => {
    return <VideoItem onVideoSelect={onVideoSelect} key={video.etag} video={video} />;
  });

  return <div className="image-list">{video}</div>;
};

export default VideoList;
